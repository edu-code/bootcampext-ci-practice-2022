package org.example

class Cicd implements Serializable {
    
    protected final script
    String currentStage = 'checkout' // possible: checkout, build, test, release, deploy
    String currentStatus = 'pending' // possible: running, cancelled, success, failed, skipped
    String imageName
    String tag
    String gitlabUrl // 'https://jenkins-cloud.nexign.com:8443/'


    /**
     * This constructor sets the Jenkins 'script' class as the local script
     * variable in order to resolve execution steps.(sh, withCredentials, etc)
     * @param script the script object
     */
    Cicd(script, String gitlabUrl) {
        this.script = script
        this.gitlabUrl = gitlabUrl
    }

    void init(){
        this.imageName = "${this.script.env.gitlabSourceRepoHomepage}".replace(this.gitlabUrl,'').toLowerCase()
        this.tag = "${this.script.env.gitlabTargetBranch}".replaceAll(/\s/,"_").toLowerCase().replaceAll(/\//,"-")
    }

    void customGitlabStage(String stageName, Closure stageSteps){
        currentStage = stageName
        currentStatus = 'pending'

        this.script.updateGitlabCommitStatus name: currentStage, state: currentStatus

        this.script.stage(currentStage){
            stageSteps()
        }
        currentStatus = 'success'
        this.script.updateGitlabCommitStatus name: currentStage, state: currentStatus
    }

    void checkout(){
         this.script.checkout([$class: 'GitSCM', 
                    branches: [[name: "${this.script.env.gitlabBranch}"]], 
                    extensions: [], userRemoteConfigs: 
                    [[credentialsId: 'deploy_user', 
                    url: this.script.env.gitlabSourceRepoHttpUrl]]
                    ])

    }




}
