import org.example.Cicd

void call() {
    updateGitlabCommitStatus name: 'checkout', state: 'pending'
    Cicd cicdHelper = new Cicd(this, 'https://jenkins-cloud.nexign.com:8443/')
    cicdHelper.init()
    String registryUrl = 'jenkins-cloud.nexign.com:5050'
    env.APP_IMAGE="${cicdHelper.imageName}:latest"
    ansiColor('xterm') {
        timestamps {
            try {
                node() {
                    cicdHelper.customGitlabStage('checkout') {
                        cicdHelper.checkout()
                    }

                    if (gitlabTargetBranch != 'main' || gitlabActionType != 'PUSH') {
                        cicdHelper.customGitlabStage('verify') {
                            docker.image('projectatomic/dockerfile-lint').inside {
                                sh 'dockerfile_lint -p -f Dockerfile'
                            }

                            docker.image('python:3.9').inside {
                                sh 'make check'
                            }
                        }

                        cicdHelper.customGitlabStage('build') {
                            docker.withRegistry("https://${registryUrl}", 'docker_user_id') {
                                // TODO: parse base_image
                                sh "docker build --cache-from ${registryUrl}/${cicdHelper.imageName}:latest --pull -t ${registryUrl}/${cicdHelper.imageName}:latest ."
                            }
                        }

                        if (gitlabTargetBranch.contains('develop') && gitlabActionType == 'PUSH') {
                            cicdHelper.customGitlabStage('release') {
                                docker.withRegistry("https://${registryUrl}", 'docker_user_id') {
                                    sh "docker tag ${registryUrl}/${cicdHelper.imageName}:latest ${registryUrl}/${cicdHelper.imageName}:${cicdHelper.tag}"
                                    sh "docker push ${registryUrl}/${cicdHelper.imageName}"
                                }
                            }
                        }
                    }else {
                            cicdHelper.customGitlabStage('deploy') {
                                withCredentials([usernamePassword(credentialsId: 'docker_user_id', passwordVariable: 'DEPLOY_PASSWORD',     usernameVariable: 'DEPLOY_USER')]) {
                                    sh 'cd ansible && ansible-playbook -v playbook.yml'
                                }
                            }
                    }
                }
                currentBuild.result = 'SUCCESS'
            } catch (ex) {
                currentBuild.result = 'FAILURE'
                cicdHelper.currentStatus = 'failed'
                throw ex
            } finally {
                //TODO:
                // sh 'docker rmi'
                updateGitlabCommitStatus name: cicdHelper.currentStage, state: cicdHelper.currentStatus
                String smile = (currentBuild.result == 'SUCCESS') ? ':+1:' : ':-1:'
                message = "${currentBuild.rawBuild.getAbsoluteUrl()} ${smile}"
                addGitLabMRComment comment: message
            }
        }
    }
}
