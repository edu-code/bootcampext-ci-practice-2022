### python

```
pip3 install -U --user pip
pip3 install -U --user ansible docker-compose
```

### Makefile

```
PATH:=${PATH}:~/.local/bin

deploy:
	cd ansible && ansible-playbook -vvv playbook.yml -t deploy

stop:
	cd ansible && ansible-playbook -vvv playbook.yml -t stop --skip-tags configure

start:
	cd ansible && ansible-playbook -vvv playbook.yml -t start --skip-tags configure
```

## Jenkinsfile
```
String imageName = 'test_image' 																																												\\ Строковый параметр, хранящий имя тега ("${gitlabTargetBranch}")
updateGitlabCommitStatus name: 'build', state: 'pending'                                                                \\ Функция для обновления статуса пайплайна в Gitlab
node{																																																										\\ Функция для описания агента, на котором запустимся
	stage('build'){																																																				\\ Имя стадии
		checkout([																																																					\\ Функция клонирования репозитория
			$class: 'GitSCM', 
								branches: [[name: "${gitlabBranch}"]], 																																	\\ подстановка параметра, полученного от gitlab-plugin
				extensions: [], userRemoteConfigs: 
				[[credentialsId: 'deploy_user', 
				url: 'https://jenkins-cloud.nexign.com:8443/red00/bootcampext-git-practice-2022.git']
				]
		])
		
		docker.withRegistry("https://${registryUrl}", 'docker_user_id')																											\\ авторизация в docker registry
		{
		sh "docker build --cache-from ${registryUrl}/${imageName}:latest --pull -t ${registryUrl}/${imageName}:latest ."
		sh "docker push ${registryUrl}/${imageName}"
		}
	}
}
updateGitlabCommitStatus name: 'build', state: 'success'
```
