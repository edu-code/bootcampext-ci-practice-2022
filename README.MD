## Practice for CI/CD

0. Авторизоваться на `https://jenkins-cloud.nexign.com/` под пользователем `redXX`
1. Выполнить **homework**:
  - ознакомиться с материалами из конференций:
    - https://www.youtube.com/watch?v=5nzTMvG62EA
    - https://www.youtube.com/watch?v=lBZaj09Qzow
  - Сгенерировать для пользователя Gitlab ssh ключи (если не сделали ранее). [Инструкция]( https://jenkins-cloud.nexign.com:8443/edu-code/bootcampext-git-practice-2022/-/blob/main/ssh-key.md)
  - разобраться с работой утилиты [make](https://habr.com/ru/company/macloud/blog/566046/)
  - на стенде установить [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#pip-install), docker-compose
  в вашего пользователя, используя `pip3`
  - **попытаться написать простой playbook,
  который запустит собранное приложение в виде сервиса (использовать docker-service)** *
  - или взять уже готовый из **examples**
  - Если используете готовый - поменяйте значение параметра `app_port` с `00` на `XX`, где `XX` часть `redXX`
  - ознакомиться со статьей по [Jenkins pipelines](https://habr.com/ru/company/yoomoney/blog/538664/)
2. Форкнуть репозиторий с практикой по Git в свое пространство в Gitlab с internal правами
3. Настроить проект:
   1. Удалить ссылку на основной проект (`Settings -> General -> Advanced`)
   2. Настроить merge requests (`Settings -> General ->Merge checks`):
      1. Pipelines must succeed
      2. Skipped pipelines are considered successful
4. В интерфейсе Jenkins, в своей директории (`redXX`), создать каталог с наименованием репозитория: `bootcampext-git-practice-2022`;
5. В данном каталоге создать job вида pipeline, с именем `ci` и сохранить его;
6. Создать конфигурацию для Gitlab webhook по [инструкции](https://about.gitlab.com/handbook/customer-success/demo-systems/tutorials/integrations/create-jenkins-pipeline/):
   1. Установить галку напротив Build configuration -> Build when a change is pushed to GitLab.
   2. Привести конфигурацию в соответствие с изображением:
     ![JenkinsWebhook1](images/JenkinsWebhook1.PNG)
     ![JenkinsWebhook2](images/JenkinsWebhook2.PNG)
     Конфигурация:
        - **Comment: jenkins_retry**  - позволяет выполнять перезапуск пайплайн из gitlab MR
        - Filter branches by regex:
          - **Source Branch Regex: ^(main|(hotfix|feature|develop)\/.\*)$**   - позволяет запускаться только в случае если исходная ветка удовлетворяет условию
          - **Target Branch Regex: ^(main|develop\/.\*|feature\/.\*|feature\/.\*|hotfix\/.\*)$** - запускается если в Merge Request, target ветка удовлетворяет условию.
   3. Настраиваем конфигурацию Gitlab Webhhok:
      1. Копируем адрес хука из Build configuration -> Build when a change is pushed to GitLab. GitLab webhook URL.
      2. Добовляем webhook в Gitlab, согласно описанию ниже(Setting->Webhooks-> Add webhook)
      **Обязательно меняем имя хоста в url с  https://jenkins-cloud.nexign.com/ на  https://62.84.121.234/**
      
      ![GitlabWebhook](images/WebhookGitlab2.png)
7.  В Definition пайлайна написать CI пайплайн, на основе содержимого **.gitlab-ci** (файл .gitlab-ci удалить) (*)  или воспользоваться готовым примером из Jenkinsfile
8.  Перенести Definition в файл Jenkinsfile, который будет храниться у вас в репозтории и подключить его через Pipeline Scm.
9.  Добавить возможность запуска установки приложения, через ansible (см examples) при пуше в main.
10. Для установки сервиса использовать ansible модуль docker-compose:
    1.  Имя проекта = имени пользователя (`red0XX`)
    2.  Имя сервиса - bootcamp_app
    3.  Порт, который будет пробрасываться - `91XX`, где *XX* - последние цифры пользователя `red0XX`
    4.  По резудьтату выполнения запуска docker-compose должен быть поднят контейнер с именем: `red0XX_bootcamp_app_1` и портом `91XX -> 8000`
12. Добавить в пайплайн шаг установки контейнера, через ansible, при пуше в main ветку из develop. (Если не воспользовались готовым пайплайном)
13. Проверить работу jenkins/Gitlab pipelines (на каждом этапе проверяя работу пайплайна) используя следующий подход:
    1. Создать ветку feature/cicd и внести в нее изменения при помощи push
    2. С ветки main создать ветку develop/1.1.1
    3. Отправить MR(Merge Request) в ветку develop/1.1.1
    4. Принять MR
    5. Отправить MR(Merge Request) из ветки develop/1.1.1 в ветку main
    6. Принять MR
14. **Перенести основной код пайплайна в JSL(Jenkins Shared Library) и подключить его к проекту.** (*):
    1.  Подключать JSL стоит в каталоге `bootcampext-git-practice-2022`
    2.  С именем `cicd`
    3.  Подключать в пайплайне через аннотацию: `@Library('cicd@main') _` в самом начале пайплайна.
    4.  При появлении надписи `org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException: Scripts not permitted to use...` напишите преподавателю, со ссылкой на билд, чтобы он как администратор системы добавил метод или функцию в исключения.


### З.Ы.

Убедительная просьба соблюдать наименование, там где оно явно указано,
поскольку все проверки на наличие обязательных шагов производятся в автоматизированном режиме.
